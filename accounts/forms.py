from .models import User
from django import forms

from django.core.exceptions import NON_FIELD_ERRORS
# 폼은 html에 태그인데 프론트에서 사용자의 입력을 받는 인터페이스
# 장고의 폼 : HTML의 폼 역할, 데이터베이스에 저장할 내용을 형식, 제약조건을 결정하게됨


class RegisterForm(forms.ModelForm):
    CHOICES = [('L', '좌파'),
               ('C', '중도'),
               ('R', '우파')]
    user_id = forms.CharField(label='아이디', widget=forms.TextInput(
        attrs={'id':'user_id', 'class':'check', 'placeholder':'아이디를 입력해주세요.'}))
    nickname = forms.CharField(label='닉네임', widget=forms.TextInput(
        attrs={'id':'nickname', 'class':'check', 'placeholder':'닉네임을 입력해주세요.'}))
    like = forms.ChoiceField(label='정치성향', choices=CHOICES, widget=forms.RadioSelect)
    email = forms.EmailField(label='이메일', widget=forms.EmailInput(
        attrs={'id':'email', 'class':'check', 'placeholder':'이메일을 입력해주세요.'}))
    password = forms.CharField(label='비밀번호', widget=forms.PasswordInput(attrs={'id':'password'}))  # 비밀번호, widget의 저 값은 ***나오게하는 것
    password2 = forms.CharField(label='비밀번호 확인', widget=forms.PasswordInput(attrs={'id':'password2'}))  # 비밀번호 확인

    class Meta:
        model = User
        fields = ['user_id', 'nickname', 'like', 'email']

    def clean_user_id(self):
        cd = self.cleaned_data
        user = User.objects.filter(user_id=['user_id']).exists()
        if user:
            raise forms.ValidationError("이미 있는 아이디 입니다.")
        else:
            return cd['user_id']

    def clean_nickname(self):
        cd = self.cleaned_data
        nickname = User.objects.filter(nickname=['nickname']).exists()
        if nickname:
            raise forms.ValidationError("이미 있는 닉네임 입니다.")
        else:
            return cd['user_id']

    def clean_email(self):
        cd = self.cleaned_data
        email = User.objects.filter(email=['email']).exists()
        if email:
            raise forms.ValidationError("이미 있는 email 입니다.")
        else:
            return cd['email']

    def clean_password2(self):
        cd = self.cleaned_data  # sql 인잭션을 해놓은 data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('비밀번호가 일치하지 않습니다.')
        return cd['password2']

    def clean_like(self):
        cd = self.cleaned_data
        return cd['like']



